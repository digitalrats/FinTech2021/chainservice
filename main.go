package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	l "./ledger-api"
)

//creating smartcontract chaincode

//initailizing smart contract structure
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func CreateGeneric(w http.ResponseWriter, r *http.Request) {
	var GenericContract l.SmartContract
	err := json.NewDecoder(r.Body).Decode(&GenericContract)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	l.CreateGeneric()
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.CreateGeneric("/GenCreate", CreateGeneric())
	log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
	handleRequests()
}
