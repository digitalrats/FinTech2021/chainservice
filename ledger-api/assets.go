package ledgerapi

type SmartContract struct {
	Type         string `json:"Type"` //smartcontract type
	ID           string `json:"ID"`
	ContrName    string `json:"ContrName"`
	Inn          int    `json:"Inn"`
	Kpp          int    `json:"Kpp"`
	Bill         int    `json:"Bill"`
	BankBill     int    `json:"BankBill"`
	Bik          int    `json:"Bik"`
	ContractText string `json:"ContractName"`
	Accepted     bool   `json:"Accepted"`
	Mgt          int    `json:"Mgt"`
	PayTime      int    `json:"PayTime"`
	Peny         int    `json:"Peny"`
	Value        int    `json:"Value"`
	Rangfe       int    `json:"Range"`
}
