package ledgerapi

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type ServiceChaincode struct {
	contractapi.Contract
}

const index = "Contr~Name"

func (t *ServiceChaincode) CreateContract(ctx contractapi.TransactionContextInterface, sc SmartContract) error {
	exists, err := t.ContractExists(ctx, sc.ID)
	if exists {
		return fmt.Errorf("asset already exists: %s", sc.ID)
	}

	assetBytes, err := json.Marshal(sc)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(sc.ID, assetBytes)
	if err != nil {
		return err
	}

	InnIndex, err := ctx.GetStub().CreateCompositeKey(index, []string{sc.ContrName, sc.ID})
	if err != nil {
		return err
	}
	value := []byte{0x00}
	return ctx.GetStub().PutState(InnIndex, value)
}

func (t *ServiceChaincode) makeBill() {

}

func (t *ServiceChaincode) checkDate() {

}

func (t *ServiceChaincode) transferContract() {

}

func (t *ServiceChaincode) initializeContractWithPeny() {

}

func (t *ServiceChaincode) ContractExists(ctx contractapi.TransactionContextInterface, ID string) (bool, error) {
	contractBytes, err := ctx.GetStub().GetState(ID)
	if err != nil {
		return false, fmt.Errorf("Asset already exists")
	}
	return contractBytes != nil, nil

}
